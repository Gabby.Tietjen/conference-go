from .models import Presentation
from common.json import ModelEncoder
from events.api_views import ConferenceListEncoder

class PresentationListEncoder(ModelEncoder):
    model = Presentation
    properties = ["title"]

    def get_extra_data(self, o):
        return {"state": o.status.name}

class PresentationDetailEncoder(ModelEncoder):
    model = Presentation
    properties = [
        "title",
        "presenter_name",
        "company_name",
        "presenter_email",
        "synopsis",
        "created",
        "conference",
    ]
    encoders = {
        "conference": ConferenceListEncoder()
        }

